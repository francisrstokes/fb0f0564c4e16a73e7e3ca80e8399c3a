const { parse, char, str, sequenceOf, choice } = require('arcsecond');

// parse creates a function we can use for parsing text
const parseText = parse (combinedParser);

console.log(
  parseText ('hello world') 
)
// -> [ 'hello', ' ', 'world' ]